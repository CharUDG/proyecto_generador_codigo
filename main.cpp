/*
****************************************************************************************
Alumno: Ramirez Navarro Carlos Eduardo
Prof. MICHEL EMANUEL LOPEZ FRANCO
****************************************************************************************
*/

#include <string>
#include <stdio.h>
#include <conio.h>
#include "arbol.h"
#include <cstdlib>

using namespace std;

//Genera el c�digo ensamblador
string GeneraCodigo(Nodo *nodo)
{
    string codigoNodo = nodo->GeneraCodigo();
    string codigo;
    codigo  = ".386 \n";
    codigo += ".model flat, stdcall \n";
    codigo += "option casemap:none ;labels are case-sensitive now \n\n";

    codigo += "include .\\masm32\\macros\\macros.asm \n";
    codigo += "include .\\masm32\\include\\masm32.inc \n";
    codigo += "include .\\masm32\\include\\kernel32.inc \n";
    codigo += "includelib .\\masm32\\lib\\masm32.lib \n";
    codigo += "includelib .\\masm32\\lib\\kernel32.lib \n\n";

    codigo += Nodo::Separador(";============== <Variables> ============= \n");
    codigo += ".data? \n";
    codigo += Nodo::codigoData;
    codigo += Nodo::Separador(";============ <Fin Variables> =========== \n\n");

    codigo += Nodo::Separador(";================ <Codigo> ============== \n");
    codigo += "\n.code \n";
    codigo += "inicio: \n\n";
    codigo += codigoNodo;

    codigo += "\n    exit\n";
    codigo += "END inicio \n";
    codigo += Nodo::Separador(";============== <Fin Codigo> ============ \n");
    return codigo;
}

//================================================================================

//Crea el archivo con el c�digo ensamblador dado
void GeneraArchivoAsm(string codigo)
{
    FILE *archivo;

    //Abre el archivo con permisos de escritura, o si no existe primero lo crear�
    archivo = fopen("codigo.asm", "w");

    //Si el archivo no pudo abrirse
    if (archivo == NULL)
    {
        cout << "El archivo no pudo crearse" << endl;
    }
    else
    {
        //fprintf no recibe string sino const char *, por lo que necesitamos
        //convertir la cadena a un arreglo de caracteres
        char c[codigo.length() - 1];
        for (int i = 0; i < codigo.length(); i++)
        {
            c[i] = codigo[i];
        }
        c[codigo.length()] = '\0';

        //Escribimos el c�digo ensamblador en el archivo
        fprintf(archivo, c);

        //Cerramos el archivo
        fclose(archivo);
    }
}

//================================================================================

//Ejecuta el an�lisis sem�ntico, y la generaci�n de c�digo, para un �rbol dado
void Ejecuta(Nodo *arbol)
{
    //Limpiamos datos internos de los nodos
    Nodo::Reinicia();

    //Configuraci�n b�sica de formato para la generaci�n de c�digo
    Nodo::usarSeparadores = true;
    Nodo::usarComentarios = true;

    //Que queremos mostrar de todo el proceso?
    bool mostrarArbolSintactico = false;
    bool mostrarAnalisisSemantico = true;
    bool mostrarCodigoEnsamblador = true;

    if (mostrarArbolSintactico)
    {
        cout << endl;
        cout << "*********************************************" << endl;
        cout << "************* Arbol sintactico **************" << endl;
        cout << "*********************************************" << endl;
        cout << endl;

        arbol->Muestra();
    }

    if (mostrarAnalisisSemantico)
    {
        cout << endl;
        cout << "*********************************************" << endl;
        cout << "************ Analisis semantico *************" << endl;
        cout << "*********************************************" << endl;
        cout << endl;

        //Generamos el an�lisis sem�ntico
        arbol->ValidaTipos();

        //Si no hubo errores sem�nticos
        if (Nodo::error == false)
            cout << "Analisis semantico exitoso!" << endl;
    }

    if (mostrarCodigoEnsamblador)
    {
        //Si no hubo errores sem�nticos
        if (Nodo::error == false)
        {
            cout << endl;
            cout << "*********************************************" << endl;
            cout << "*************** Ensamblador *****************" << endl;
            cout << "*********************************************" << endl;
            cout << endl;

            //Ejecutamos la generaci�n de c�digo
            string codigo = GeneraCodigo(arbol);

            //Mostramos el c�digo generado
            cout << codigo;

            //Y creamos el archivo .asm
            GeneraArchivoAsm(codigo);
        }
        else
        {
            cout <<endl << "No se pudo generar el codigo ensamblador ya que el programa contiene errores semanticos" << endl;
        }
    }
}

//================================================================================

//Obtiene el �rbol especificado por la cadena dada
Nodo *SeleccionaArbol(string seleccion)
{
    if (seleccion == "0")
    {
        return Arboles::Arbol0();
    }
    else if (seleccion == "1")
    {
        return Arboles::Arbol1();
    }
    else if (seleccion == "2")
    {
        return Arboles::Arbol2();
    }
    else if (seleccion == "3")
    {
        return Arboles::Arbol3();
    }
    else if (seleccion == "4")
    {
        return Arboles::Arbol4();
    }
    else if (seleccion == "5")
    {
        return Arboles::Arbol5();
    }
    else if (seleccion == "6")
    {
        return Arboles::Arbol6();
    }
    else if (seleccion == "7")
    {
        return Arboles::Arbol7();
    }
    else if (seleccion == "7v2")
    {
        return Arboles::Arbol7v2();
    }
    else if (seleccion == "8")
    {
        return Arboles::Arbol8();
    }
    else if (seleccion == "8v2")
    {
        return Arboles::Arbol8v2();
    }
    else if (seleccion == "9")
    {
        return Arboles::Arbol9();
    }
    else if (seleccion == "10")
    {
        return Arboles::Arbol10();
    }
    else if (seleccion == "10v2")
    {
        return Arboles::Arbol10v2();
    }
    else if (seleccion == "0e")
    {
        return Arboles::Arbol0e();
    }
    else if (seleccion == "1e")
    {
        return Arboles::Arbol1e();
    }
    else if (seleccion == "2e")
    {
        return Arboles::Arbol2e();
    }
    else if (seleccion == "3e")
    {
        return Arboles::Arbol3e();
    }
    else
    {
        return NULL;
    }
}

//================================================================================

int main()
{
    string seleccion;
    Nodo *arbol;
    do
    {
        system("CLS");
        seleccion = "";

        cout << "Que arbol deseas ejecutar?: ";
        cin >> seleccion;

        //Obtenemos el �rbol que el usuario seleccione
        arbol = SeleccionaArbol(seleccion);

        //Si se selecciono un �rbol valido
        if (arbol != NULL)
            Ejecuta(arbol);
        else
            cout << endl << "Opcion invalida!" << endl;

        cout << endl << "---------------------------------------------------" << endl;
        cout << "Quieres continuar (s/n): ";
        cin >> seleccion;
        cout << endl;
    }while (seleccion == "S" || seleccion == "s");

    return 0;
}
