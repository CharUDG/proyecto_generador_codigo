/*
****************************************************************************************
Alumno: Ramirez Navarro Carlos Eduardo
Prof. MICHEL EMANUEL LOPEZ FRANCO
****************************************************************************************
*/

#ifndef _ARBOL
#define _ARBOL

#include <string>
#include <iostream>
#include <vector>

using namespace std;

//Diferentes tipos aceptados durante el an�lisis sem�ntico
enum TiposSemanticos
{
    ENTERO,
    REAL,
    VACIO,
    ERROR
};

//================================================================================

//Representa un elemento de la tabla sem�ntica, el cual contiene un simbolo y el
//tipo sem�ntico relacionado al mismo
class ElementoTabla
{
    public:
        TiposSemanticos tipo;
        string simbolo;

        //------------------------------------------------------------------------

        //Constructor
        ElementoTabla(TiposSemanticos tipo, string simbolo)
        {
            this->tipo = tipo;
            this->simbolo = simbolo;
        }
};

//================================================================================

//La tabla sem�ntica usada para almacenar simbolos encontrados durante el an�lisis
//sem�ntico
class TablaSemantica
{
    public:
        vector<ElementoTabla*> tabla;

        //------------------------------------------------------------------------

        //Constructor
        TablaSemantica()
        {
        }

        //------------------------------------------------------------------------

        //Obtiene el elemento de la tabla que contenga el simbolo dado, regresa
        //NULL en caso de que ning�n elemento con ese simbolo sea encontrado
        ElementoTabla *ObtieneElemento(string simbolo)
        {
            //Entre todos los elementos de la tabla
            for(int i = 0; i < tabla.size(); i++)
            {
                //Si encontramos el elemento con el simbolo dado, lo regresamos
                if (tabla.at(i)->simbolo == simbolo)
                {
                    return tabla.at(i);
                }
            }

            return NULL;
        }

        //------------------------------------------------------------------------

        //Introduce el elemento dado a la tabla
        void IntroduceElemento(ElementoTabla *elemento)
        {
            tabla.push_back(elemento);
        }

        //------------------------------------------------------------------------

        //Elimina todos los elementos almacenados en la tabla
        void Limpia()
        {
            tabla.clear();
        }
};

//================================================================================

//Clase base para todos los nodos del �rbol sem�ntico
class Nodo
{
    public:
        //Almacena el nivel de sangr�a actual, usado para imprimir el �rbol
        //sint�ctico
        static int sangria;

        //Lleva la cuenta de la cantidad de etiquetas utilizadas en la generaci�n
        //de c�digo ensamblador
        static int cantEtiquetas;

        //La tabla sem�ntica usada para almacenar los simbolos encontrados en el
        //an�lisis
        static TablaSemantica *tabla;

        //�ndica si se encontr� alg�n error durante el an�lisis sem�ntico
        static bool error;

        //Almacena el c�digo de todas las definiciones de variables; necesitamos
        //separarlo de todo el dem�s c�digo para lograr establecer la estructura
        //b�sica de un programa en ensamblador (.data y .code)
        static string codigoData;

        //Formato para el c�digo ensamblador generado
        static bool usarSeparadores;
        static bool usarComentarios;

        string simbolo;
        Nodo *sig;
        TiposSemanticos tipo;

        //------------------------------------------------------------------------

        //Constructor
        Nodo()
        {
            sig = NULL;
            tipo = ERROR;
        }

        //------------------------------------------------------------------------

        //Devuelve el c�digo ensamblador para un separador dado
        static string Separador(string separador)
        {
            return Nodo::usarSeparadores ? separador : "";
        }

        //------------------------------------------------------------------------

        //Devuelve el c�digo ensamblador para un comentario dado
        static string Comentario(string comentario)
        {
            return Nodo::usarComentarios ? comentario : "";
        }

        //------------------------------------------------------------------------

        //Muestra un espacio en blanco que sirve para jerarquizar visualmente el
        //�rbol sint�ctico
        void MuestraSangria()
        {
            for (int i = 0; i < Nodo::sangria; i++)
                printf(" ");
        }

        //------------------------------------------------------------------------

        //Obtiene un etiqueta �nica para usar en el c�digo ensamblador, con un
        //nombre base dado
        string SiguienteEtiqueta(string nombreBase)
        {
            //Pasamos el numero de entero a cadena
            char numero[10];
            sprintf(numero, "_%d", cantEtiquetas++);

            return "Etq_" + nombreBase + numero;
        }

        //------------------------------------------------------------------------

        //�ndica que ocurri� un error sem�ntico
        static void Error()
        {
            Nodo::error = true;
        }

        //------------------------------------------------------------------------

        //�ndica que ocurri� un error sem�ntico, y adicionalmente imprime en
        //pantalla un mensaje que representa el error ocurrido
        static void Error(string error)
        {
            Nodo::Error();
            cout << "Error - " << error << endl;
        }

        //------------------------------------------------------------------------

        //Restablece a sus valores por default variables internas usadas por los
        //distintos an�lisis
        static void Reinicia()
        {
            Nodo::sangria = 0;
            Nodo::tabla = new TablaSemantica();
            Nodo::tabla->Limpia();
            Nodo::error = false;
            Nodo::cantEtiquetas = 0;
            Nodo::codigoData = "";
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        virtual void Muestra()
        {
        };

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        virtual void ValidaTipos()
        {
            if (sig != NULL)
                sig->ValidaTipos();
        };

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        virtual string GeneraCodigo()
        {
            return "";
        };
};

//Inicializamos datos estaticos del nodo
int Nodo::sangria = 0;
TablaSemantica *Nodo::tabla = new TablaSemantica();
bool Nodo::error = false;
int Nodo::cantEtiquetas = 0;
string Nodo::codigoData = "";
bool Nodo::usarSeparadores = false;
bool Nodo::usarComentarios = false;

//================================================================================

//Nodo que representa un tipo de dato
class Tipo: public Nodo
{
    public:
        //Constructor
        Tipo(string simbolo)
        {
            this->simbolo = simbolo;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            MuestraSangria();
            cout << "<Tipo>" << simbolo << endl;
            if (sig != NULL)
                sig->Muestra();
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            if (simbolo == "int")
                tipo = ENTERO;
            else if (simbolo == "float")
                tipo = REAL;
            else
                tipo = ERROR;
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            if (simbolo == "int")
                return "dword";
            else if (simbolo == "float")
                return "REAL4";
            else
                return "";
        }
};

//================================================================================

//Clase base para los nodos expresion
class Expresion: public Nodo
{
    protected:
        Expresion *izq, *der;

    public:
        //Constructor
        Expresion()
        {
            izq = der = NULL;
        }

        //------------------------------------------------------------------------

        //Constructor
        Expresion(Expresion *izq, Expresion *der)
        {
            this->izq = izq;
            this->der = der;
            sig = NULL;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo,
        //usando el operador dado
        void Muestra(string op)
        {
            MuestraSangria();
            cout << "<" << op << ">" << simbolo << endl;
            sangria++;
            izq->Muestra();
            der->Muestra();
            sangria--;
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            if (izq != NULL)
                izq->ValidaTipos();
            if (der != NULL)
                der->ValidaTipos();
        }
};

//================================================================================

//Nodo que representa un identificador
class ID: public Expresion
{
    public:
        //Constructor
        ID(string simbolo, ID *id = NULL)
        {
            this->simbolo = simbolo;
            sig = id;
        }

        //------------------------------------------------------------------------

        //Transforma el simbolo dado en un simbolo auxiliar que no de conflictos
        //con palabras reservadas (o macros definidas) propias del MASM32, y es que
        //existen identificadores simples (tales como "c") que ya estan predefinidos
        //en MASM32 por lo que esta funci�n debe de usarse siempre al manipular
        //internamente cualquier variable definida por el usuario, y s�lo usar el
        //nombre de la variable original con propositos de referencial visual para
        //con el usuario (en mensajes y dem�s)
        static string SimboloAux(string simbolo)
        {
            return "_" + simbolo;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            MuestraSangria();
            cout << "<Identificador>" << simbolo << endl;
            if (sig != NULL)
                sig->Muestra();
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            //Buscamos el simbolo en la tabla
            ElementoTabla *elemento = Nodo::tabla->ObtieneElemento(ID::SimboloAux(simbolo));

            //Establecemos el tipo del elemento
            tipo = elemento != NULL ? elemento->tipo : ERROR;

            //Si no se encontro en la tabla
            if (tipo == ERROR)
                Error("Uso de la variable no definida : " + simbolo);
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            return "    push " + ID::SimboloAux(simbolo) + "\n";
        }
};

//================================================================================

//Nodo que representa un entero
class Entero: public Expresion
{
    public:
        //Constructor
        Entero(string simbolo)
        {
            this->simbolo = simbolo;
            sig = NULL;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            MuestraSangria();
            cout << "<Entero>" << simbolo << endl;
            if (sig != NULL)
                sig->Muestra();
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            tipo = ENTERO;
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            return "    push " + simbolo + "\n";
        }
};

//================================================================================

//Nodo que representa un n�mero flotante
class Real: public Expresion
{
    public:
        //Constructor
        Real(string simbolo)
        {
            this->simbolo = simbolo;
            sig = NULL;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            MuestraSangria();
            cout << "<Real>" << simbolo << endl;
            if (sig != NULL)
                sig->Muestra();
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            tipo = REAL;
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            return "    push " + simbolo + "\n";
        }
};

//================================================================================

//Nodo que representa una expresi�n con signo
class Signo: public Expresion
{
    public:
        //Constructor
        Signo(Expresion *izq)
            :Expresion(izq,NULL)
        {
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            MuestraSangria();
            cout << "<" << "Signo" << ">" << simbolo << endl;
            sangria++;
            izq->Muestra();
            sangria--;
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            //Validamos el tipo de la expresi�n hija
            Expresion::ValidaTipos();

            tipo = izq->tipo;

            if (tipo == ERROR)
                //Error("Expresi�n con signo invalida");
                Error();
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string codigo;
            codigo  = Separador("    ;============== <Signo> ============= \n");
            codigo += izq->GeneraCodigo();
            codigo += "    pop eax \n";
            codigo += "    mov ebx, -1 \n";
            codigo += "    imul ebx \n";
            codigo += "    push eax \n";
            codigo += Separador("    ;============ <Fin Signo> =========== \n");
            return codigo;
        }
};

//================================================================================

//Nodo que representa una suma entre expresiones
class Suma: public Expresion
{
    public:
        //Constructor
        Suma(Expresion *izq, Expresion *der)
            :Expresion(izq,der)
        {
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            Expresion::Muestra("Suma");
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            //Validamos los tipos de expresiones hijas
            Expresion::ValidaTipos();

            tipo = izq->tipo == der->tipo ? izq->tipo : ERROR;

            if (tipo == ERROR)
                //Error("Suma de expresiones invalidas");
                Error();
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string codigo;
            codigo  = Separador("    ;============== <Suma> ============== \n");
            codigo += izq->GeneraCodigo();
            codigo += der->GeneraCodigo();
            codigo += "    pop ebx \n";
            codigo += "    pop eax \n";
            codigo += "    add eax, ebx \n";
            codigo += "    push eax \n";
            codigo += Separador("    ;============ <Fin Suma> ============ \n");
            return codigo;
        }
};

//================================================================================

//Nodo que representa una multiplicaci�n entre expresiones
class Mult: public Expresion
{
    public:
        //Constructor
        Mult(Expresion *izq, Expresion *der)
            :Expresion(izq,der)
        {
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            Expresion::Muestra("Mult");
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            //Validamos los tipos de expresiones hijas
            Expresion::ValidaTipos();

            tipo = izq->tipo == der->tipo ? izq->tipo : ERROR;

            if (tipo == ERROR)
                //Error("Multiplicacion de expresiones invalidas");
                Error();
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string codigo;
            codigo  = Separador("    ;============== <Mult> ============== \n");
            codigo += izq->GeneraCodigo();
            codigo += der->GeneraCodigo();
            codigo += "    pop ebx \n";
            codigo += "    pop eax \n";
            codigo += "    imul ebx \n";
            codigo += "    push eax \n";
            codigo += Separador("    ;============ <Fin Mult> ============ \n");
            return codigo;
        }
};

//================================================================================

//Nodo que representa un And entre expresiones
class Conjuncion: public Expresion
{
    public:
        //Constructor
        Conjuncion(Expresion *izq, Expresion *der)
            :Expresion(izq,der)
        {
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            Expresion::Muestra("Conj");
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            //Validamos los tipos de expresiones hijas
            Expresion::ValidaTipos();

            tipo = (izq->tipo == der->tipo) && (izq->tipo == ENTERO) ? izq->tipo : ERROR;

            if (tipo == ERROR)
                Error("El operador And no puede aplicarse a expresiones de ese tipo");
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string etqFalso = SiguienteEtiqueta("AndFalso");
            string etqFin = SiguienteEtiqueta("AndFin");
            string codigo;
            codigo  = Separador("    ;============= <And> ============ \n");
            codigo += izq->GeneraCodigo();
            codigo += der->GeneraCodigo();
            codigo += "    pop ebx \n";
            codigo += "    pop eax \n\n";

            //Si la expresion izq es falsa
            codigo += Comentario("    ; Si la expresion izq es falsa \n");
            codigo += "    cmp eax, 0 \n";
            codigo += "    je " + etqFalso + " \n\n";

            //Si la expresion der es falsa
            codigo += Comentario("    ; Si la expresion der es falsa \n");
            codigo += "    cmp ebx, 0 \n";
            codigo += "    je " + etqFalso + " \n\n";

            //Parte verdadera
            codigo += Comentario("    ; Parte verdadera \n");
            codigo += "    push 1 \n";
            codigo += "    jmp " + etqFin + " \n\n";

            //Parte falsa
            codigo += Comentario("    ; Parte falsa \n");
            codigo += "    " + etqFalso + ": \n";
            codigo += "    push 0 \n\n";

            codigo += "    " + etqFin + ": \n";
            codigo += Separador("    ;============= <Fin And> ============ \n");
            return codigo;
        }
};

//================================================================================

//Nodo que representa un Or entre expresiones
class Disyuncion: public Expresion
{
    public:
        //Constructor
        Disyuncion(Expresion *izq, Expresion *der)
            :Expresion(izq,der)
        {
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            Expresion::Muestra("Disy");
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            //Validamos los tipos de expresiones hijas
            Expresion::ValidaTipos();

            tipo = (izq->tipo == der->tipo) && (izq->tipo == ENTERO) ? izq->tipo : ERROR;

            if (tipo == ERROR)
                Error("El operador Or no puede aplicarse a expresiones de ese tipo");
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string etqVerdadero = SiguienteEtiqueta("OrVerdadero");
            string etqFin = SiguienteEtiqueta("OrFin");
            string codigo;
            codigo  = Separador("    ;=============== <Or> =============== \n");
            codigo += izq->GeneraCodigo();
            codigo += der->GeneraCodigo();
            codigo += "    pop ebx \n";
            codigo += "    pop eax \n\n";

            //Si la expresion izq es verdadera
            codigo += Comentario("    ; Si la expresion izq es verdadera \n");
            codigo += "    cmp eax, 0 \n";
            codigo += "    jne " + etqVerdadero + " \n\n";

            //Si la expresion der es verdadera
            codigo += Comentario("    ; Si la expresion der es verdadera \n");
            codigo += "    cmp ebx, 0 \n";
            codigo += "    jne " + etqVerdadero + " \n\n";

            //Parte falsa
            codigo += Comentario("    ; Parte falsa \n");
            codigo += "    push 0 \n";
            codigo += "    jmp " + etqFin + " \n\n";

            //Parte verdadera
            codigo += Comentario("    ; Parte verdadera \n");
            codigo += "    " + etqVerdadero + ": \n";
            codigo += "    push 1 \n\n";

            codigo += "    " + etqFin + ": \n";
            codigo += Separador("    ;=============== <Fin Or> =============== \n");
            return codigo;
        }
};

//================================================================================

//Nodo que representa una relaci�n (<, >, <=, etc) entre expresiones
class Relacional: public Expresion
{
    public:
        //Constructor
        Relacional(string simbolo, Expresion *izq, Expresion *der)
            :Expresion(izq,der)
        {
            this->simbolo = simbolo;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            Expresion::Muestra("Relacional");
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            //Validamos los tipos de expresiones hijas
            Expresion::ValidaTipos();

            tipo = (izq->tipo == der->tipo) && (izq->tipo == ENTERO) ? izq->tipo : ERROR;

            if (tipo == ERROR)
                Error("El operador " + simbolo + " no puede aplicarse a expresiones de ese tipo");
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string etqVerdadero = SiguienteEtiqueta("RelacVerdadero");
            string etqFin = SiguienteEtiqueta("RelacFin");
            string codigo;
            codigo  = Separador("    ;============== <Relac> ============= \n");
            codigo += izq->GeneraCodigo();
            codigo += der->GeneraCodigo();
            codigo += "    pop ebx \n";
            codigo += "    pop eax \n\n";

            //Si la relacion entre las 2 expresiones es verdadera
            codigo += Comentario("    ; Si la relacion entre las 2 expresiones es verdadera \n");
            codigo += "    cmp eax, ebx \n";
            codigo += "    " + CodigoOperador() + " " + etqVerdadero + " \n\n";

            //Parte falsa
            codigo += Comentario("    ; Parte falsa \n");
            codigo += "    push 0 \n";
            codigo += "    jmp " + etqFin + " \n\n";

            //Parte verdadera
            codigo += Comentario("    ; Parte verdadera \n");
            codigo += "    " + etqVerdadero + ": \n";
            codigo += "    push 1 \n\n";

            codigo += "    " + etqFin + ": \n";
            codigo += Separador("    ;============ <Fin Relac> =========== \n");
            return codigo;
        }

        //------------------------------------------------------------------------

        //Obtiene el c�digo ensamblador que representa el operador actual
        string CodigoOperador()
        {
            if (simbolo == "<")
                return "jl";
            else if (simbolo == ">")
                return "jg";
            else if (simbolo == "<=")
                return "jle";
            else if (simbolo == ">=")
                return "jge";
            else if (simbolo == "==")
                return "je";
            else if (simbolo == "!=")
                return "jne";
            else
                return "jmp";
        }
};

//================================================================================

//Nodo que representa una definici�n de variables
class Variables: public Nodo
{
    protected:
        Tipo *tipoDato;
        ID *identificador;
    public:
        //Constructor
        Variables(Tipo *tipoDato, ID *identificador, Nodo *sig = NULL)
        {
            this->tipoDato = tipoDato;
            this->identificador = identificador;
            this->sig = sig;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            cout << "<Variables>" << endl;
            sangria++;
            tipoDato->Muestra();
            identificador->Muestra();
            sangria--;
            if (sig != NULL)
                sig->Muestra();
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            tipoDato->ValidaTipos();
            tipo = tipoDato->tipo;

            //Por cada variable en la lista de variables
            Nodo *aux = identificador;
            while (aux != NULL)
            {
                //Buscamos la variable en la tabla
                ElementoTabla *elemento = Nodo::tabla->ObtieneElemento(ID::SimboloAux(aux->simbolo));

                //Si ya existe
                if (elemento != NULL)
                {
                    Error("Ya se ha definido una variable con el identificador: " + aux->simbolo);
                    tipo = aux->tipo = ERROR;
                }
                else
                {
                    //Introducimos la variable en la tabla
                    Nodo::tabla->IntroduceElemento(new ElementoTabla(tipo, ID::SimboloAux(aux->simbolo)));
                    aux->tipo = tipo;
                }

                //Obtenemos la siguiente variable en la lista de variables
                aux = aux->sig;
            }

            //Validamos los tipos de nodos siguientes
            Nodo::ValidaTipos();
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string t = tipoDato->GeneraCodigo();
            string codigo;

            //Por cada variable en la lista de variables
            Nodo *aux = identificador;
            while (aux != NULL)
            {
                //Le concatenamos el c�digo de esta variable a codigoData
                Nodo::codigoData += ID::SimboloAux(aux->simbolo) + " " + t + " ? \n";

                //Obtenemos la siguiente variable en la lista de variables
                aux = aux->sig;
            }

            //Obtenemos el c�digo de nodos siguientes
            if (sig != NULL)
                codigo += sig->GeneraCodigo();

            return codigo;
        }
};

//================================================================================

//Nodo que representa una asignaci�n de una expresi�n a una variable
class Asignacion: public Nodo
{
    protected:
        ID *identificador;
        Expresion *expresion;

    public:
        //Constructor
        Asignacion(ID *identificador, Expresion *expresion, Nodo *sig = NULL)
        {
            this->identificador = identificador;
            this->expresion = expresion;
            this->sig = sig;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            MuestraSangria();
            cout << "<Asignacion>" << simbolo << endl;
            sangria++;
            identificador->Muestra();
            expresion->Muestra();
            sangria--;
            if (sig != NULL)
                sig->Muestra();
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            expresion->ValidaTipos();

            //Buscamos la variable en la tabla
            ElementoTabla *elemento = Nodo::tabla->ObtieneElemento(ID::SimboloAux(identificador->simbolo));

            //Si existe
            if (elemento != NULL)
            {
                //Tiene que tener el mismo tipo que la expresi�n que se le quiere
                //agregar
                if (elemento->tipo == expresion->tipo)
                {
                    tipo = VACIO;
                }
                else
                {
                    Error("Asignacion de elementos incomplatibles");
                    ERROR;
                }
            }
            else
            {
                Error("Uso de la variable no definida " + identificador->simbolo);
                tipo = ERROR;
            }

            //Validamos los tipos de nodos siguientes
            Nodo::ValidaTipos();
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string codigo;
            codigo  = Separador("    ;=========== <Asignacion> =========== \n");
            codigo += expresion->GeneraCodigo();
            codigo += "    pop eax \n";
            codigo += "    mov " + ID::SimboloAux(identificador->simbolo) + ", eax \n";
            codigo += Separador("    ;========= <Fin Asignacion> ========= \n");

            //Obtenemos el c�digo de nodos siguientes
            if (sig != NULL)
                codigo += sig->GeneraCodigo();

            return codigo;
        }
};

//================================================================================

//Nodo que representa un if, el cual puede tener un bloque adicional para expresiones
//falsas (else)
class Si: public Nodo
{
    protected:
        Expresion *expresion;
        Nodo *bloque;
        Nodo *otro;

    public:
        //Constructor
        Si(Expresion *expresion, Nodo *bloque, Nodo *otro, Nodo *sig = NULL)
        {
            this->expresion = expresion;
            this->bloque = bloque;
            this->otro = otro;
            this->sig = sig;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            MuestraSangria();
            cout << "<Si>" << endl;
            sangria++;
            expresion->Muestra();
            bloque->Muestra();
            if (otro != NULL)
                otro->Muestra();
            sangria--;
            if (sig != NULL)
                sig->Muestra();
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            //Validamos el tipo de todas las partes que componen el if
            expresion->ValidaTipos();
            bloque->ValidaTipos();
            if (otro != NULL)
                otro->ValidaTipos();

            tipo = ERROR;

            //Verificamos que todas las partes cumplan con su tipo correspondiente
            if (expresion->tipo == ENTERO)
            {
                if (bloque->tipo == VACIO)
                {
                    if (otro != NULL && otro->tipo != VACIO)
                    {
                        Error("El bloque falso del if es invalido");
                    }
                    else
                    {
                        tipo = VACIO;
                    }
                }
                else
                {
                    Error("El bloque del if es invalido");
                }
            }
            else
            {
                Error("Expresion invalida para el if");
            }

            //Validamos los tipos de nodos siguientes
            Nodo::ValidaTipos();
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string etqOtro = SiguienteEtiqueta("IfOtro");
            string etqFinSi = SiguienteEtiqueta("IfFin");
            string codigo;
            codigo  = Separador("    ;=============== <Si> =============== \n");
            codigo += expresion->GeneraCodigo();
            codigo += "    pop eax \n\n";

            //Si la expresion es falsa
            codigo += Comentario("    ; Si la expresion es falsa \n");
            codigo += "    cmp eax, 0 \n";
            codigo += "    je " + etqOtro + " \n\n";

            //Parte verdadera
            codigo += Comentario("    ; Parte verdadera \n");
            codigo += bloque->GeneraCodigo();
            codigo += "    jmp " + etqFinSi + " \n\n";

            //Parte falsa
            codigo += Comentario("    ; Parte falsa \n");
            codigo += "    " + etqOtro + ": \n\n";
            if (otro != NULL)
                codigo += otro->GeneraCodigo();

            codigo += "\n    " + etqFinSi + ": \n";
            codigo += Separador("    ;============= <Fin Si> ============= \n");

            //Obtenemos el c�digo de nodos siguientes
            if (sig != NULL)
                codigo += sig->GeneraCodigo();

            return codigo;
        }
};

//================================================================================

//Nodo que representa un for
class Para: public Nodo
{
    protected:
        Asignacion *asignacion;
        Expresion *expresionLogica;
        Asignacion *incremento;
        Nodo *bloque;

    public:
        //Constructor
        Para(Asignacion *asignacion, Expresion *expresionLogica, Asignacion *incremento, Nodo *bloque, Nodo *sig = NULL)
        {
            this->asignacion = asignacion;
            this->expresionLogica = expresionLogica;
            this->incremento = incremento;
            this->bloque = bloque;
            this->sig = sig;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            MuestraSangria();
            cout << "<Para>" << endl;
            sangria++;
            asignacion->Muestra();
            expresionLogica->Muestra();
            incremento->Muestra();
            bloque->Muestra();
            sangria--;
            if (sig != NULL)
                sig->Muestra();
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            //Validamos el tipo de todas las partes que componen el for
            asignacion->ValidaTipos();
            expresionLogica->ValidaTipos();
            incremento->ValidaTipos();
            bloque->ValidaTipos();

            tipo = ERROR;

            //Verificamos que todas las partes cumplan con su tipo correspondiente
            if (asignacion->tipo == VACIO)
            {
                if (expresionLogica->tipo == ENTERO)
                {
                    if (incremento->tipo == VACIO)
                    {
                        if (bloque->tipo == VACIO)
                        {
                            tipo = VACIO;
                        }
                        else
                        {
                            Error("El bloque del for es invalido");
                        }
                    }
                    else
                    {
                        Error("El incremento del for es invalido");
                    }
                }
                else
                {
                    Error("Expresion logica invalida para el for");
                }
            }
            else
            {
                Error("Asignacion invalida del for");
            }

            //Validamos los tipos de nodos siguientes
            Nodo::ValidaTipos();
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string etqInicioFor = SiguienteEtiqueta("ForInicio");
            string etqFinFor = SiguienteEtiqueta("ForFin");
            string codigo;
            codigo  = Separador("    ;=============== <For> ============== \n");
            codigo += asignacion->GeneraCodigo();
            codigo += "    " + etqInicioFor + ": \n";
            codigo += expresionLogica->GeneraCodigo();
            codigo += "    pop eax \n\n";

            codigo += Comentario("    ; Si la expresion es falsa \n");
            codigo += "    cmp eax, 0 \n";
            codigo += "    je " + etqFinFor + " \n\n";

            codigo += Comentario("    ; Codigo del For \n");
            codigo += bloque->GeneraCodigo();
            codigo += incremento->GeneraCodigo();
            codigo += "    jmp " + etqInicioFor + " \n";
            codigo += "    " + etqFinFor + ": \n";
            codigo += Separador("    ;============= <Fin For> ============ \n");

            //Obtenemos el c�digo de nodos siguientes
            if (sig != NULL)
                sig->GeneraCodigo();

            return codigo;
        }
};

//================================================================================

//Nodo que representa una instrucci�n para imprimir un valor
class Imprime: public Nodo
{
    protected:
        Expresion *expresion;

    public:
        //Constructor
        Imprime(Expresion *expresion, Nodo *sig = NULL)
        {
            this->expresion = expresion;
            this->sig = sig;
        }

        //------------------------------------------------------------------------

        //Muestra en pantalla el sub-�rbol sint�ctico correspondiente a este nodo
        void Muestra()
        {
            MuestraSangria();
            cout << "<Imprime>" << endl;
            sangria++;
            expresion->Muestra();
            sangria--;
            if (sig != NULL)
                sig->Muestra();
        }

        //------------------------------------------------------------------------

        //Genera el c�digo ensamblador correspondiente a este nodo
        string GeneraCodigo()
        {
            string codigo;
            codigo  = Separador("    ;============= <Imprime> ============ \n");
            codigo += expresion->GeneraCodigo();
            codigo += "    pop eax \n";
            codigo += "    print str$( eax ) \n";
            codigo += "    print chr$( ' ', 13 ) \n";
            codigo += Separador("    ;=========== <Fin Imprime> ========== \n");

            //Obtenemos el c�digo de nodos siguientes
            if (sig != NULL)
                codigo += sig->GeneraCodigo();

            return codigo;
        }

        //------------------------------------------------------------------------

        //Genera el an�lisis sem�ntico para este nodo
        void ValidaTipos()
        {
            expresion->ValidaTipos();

            if (expresion->tipo == ENTERO)
            {
                tipo = VACIO;
            }
            else
            {
                Error("No se puede imprimir ese valor");
                tipo = ERROR;
            }

            //Validamos los tipos de nodos siguientes
            Nodo::ValidaTipos();
        }
};

//================================================================================

//Clase que contiene �rboles sint�cticos definidos
class Arboles
{
    public:
        static Nodo *Arbol0()
        {
            return new Variables( new Tipo("int"), new ID("a"));
        }

        static Nodo *Arbol1()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b", new ID("c"))));
        }

        static Nodo *Arbol2()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b")), new Variables( new Tipo("int"), new ID("c", new ID("d")), new Asignacion( new ID("a"), new Entero( "3" ) , new Imprime( new ID("a")))));
        }

        static Nodo *Arbol3()
        {
            //return new Variables( new Tipo("int"), new ID("a", new ID("b")), new Variables( new Tipo("int"), new ID("c", new ID("d")), new Asignacion( new ID("x"), new Suma( new Entero( "3" ) , new ID("c")))));
            return new Variables( new Tipo("int"), new ID("a", new ID("b")), new Variables( new Tipo("int"), new ID("c", new ID("d")), new Asignacion( new ID("c"), new Entero( "5" ) , new Asignacion( new ID("a"), new Suma( new Entero( "3" ) , new ID("c")), new Imprime( new ID("a"), new Imprime( new Suma( new Entero( "2" ) , new Mult( new Entero( "3" ) , new Signo( new Entero( "4" ) )))))))));
        }

        static Nodo *Arbol4()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b")), new Variables( new Tipo("int"), new ID("c", new ID("d")), new Asignacion( new ID("b"), new Entero( "2" ) , new Asignacion( new ID("c"), new Entero( "4" ) , new Asignacion( new ID("d"), new Entero( "1" ) , new Asignacion( new ID("a"), new Suma( new ID("b"), new Mult( new ID("c"), new ID("d"))), new Imprime( new ID("a"))))))));
        }

        static Nodo *Arbol5()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("z")), new Asignacion( new ID("z"), new Entero( "0" ) , new Asignacion( new ID("a"), new Entero( "5" ) , new Si( new Relacional(">", new ID("a"), new Entero( "2" ) ), new Asignacion( new ID("z"), new Entero( "1" ) ), NULL, new Imprime( new ID("z"))))));
        }

        static Nodo *Arbol6()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("z")), new Asignacion( new ID("a"), new Entero( "5" ) , new Si( new Relacional(">", new ID("a"), new Entero( "2" ) ), new Asignacion( new ID("z"), new Entero( "1" ) ), new Asignacion( new ID("z"), new Entero( "0" ) ), new Imprime( new ID("z")))));
        }

        static Nodo *Arbol7()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b")), new Asignacion( new ID("a"), new Entero( "5" ) , new Asignacion( new ID("b"), new Entero( "10" ) , new Si( new Conjuncion( new Relacional("<=", new ID("a"), new Entero( "10" ) ), new Relacional(">=", new ID("b"), new Entero( "20" ) )), new Asignacion( new ID("z"), new Entero( "1" ) ), new Asignacion( new ID("z"), new Entero( "0" ) ), new Imprime( new ID("z"))))));
        }

        static Nodo *Arbol7v2()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b", new ID("z"))), new Asignacion( new ID("a"), new Entero( "5" ) , new Asignacion( new ID("b"), new Entero( "10" ) , new Si( new Conjuncion( new Relacional("<=", new ID("a"), new Entero( "10" ) ), new Relacional(">=", new ID("b"), new Entero( "20" ) )), new Asignacion( new ID("z"), new Entero( "1" ) ), new Asignacion( new ID("z"), new Entero( "0" ) ), new Imprime( new ID("z"))))));
        }

        static Nodo *Arbol8()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b", new ID("c"))), new Asignacion( new ID("a"), new Entero( "5" ) , new Asignacion( new ID("b"), new Entero( "21" ) , new Asignacion( new ID("c"), new Entero( "2" ) , new Si( new Conjuncion( new Relacional("<=", new ID("a"), new Entero( "10" ) ), new Disyuncion( new Relacional(">=", new ID("b"), new Entero( "20" ) ), new Relacional("<", new ID("c"), new Entero( "10" ) ))), new Asignacion( new ID("z"), new Entero( "1" ) ), new Asignacion( new ID("z"), new Entero( "0" ) ), new Imprime( new ID("z")))))));
        }

        static Nodo *Arbol8v2()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b", new ID("c", new ID("z")))), new Asignacion( new ID("a"), new Entero( "5" ) , new Asignacion( new ID("b"), new Entero( "21" ) , new Asignacion( new ID("c"), new Entero( "2" ) , new Si( new Conjuncion( new Relacional("<=", new ID("a"), new Entero( "10" ) ), new Disyuncion( new Relacional(">=", new ID("b"), new Entero( "20" ) ), new Relacional("<", new ID("c"), new Entero( "10" ) ))), new Asignacion( new ID("z"), new Entero( "1" ) ), new Asignacion( new ID("z"), new Entero( "0" ) ), new Imprime( new ID("z")))))));
        }

        static Nodo *Arbol9()
        {
            return new Variables( new Tipo("int"), new ID("i"), new Para( new Asignacion( new ID("i"), new Entero( "0" ) ), new Relacional("<", new ID("i"), new Entero( "100" ) ), new Asignacion( new ID("i"), new Suma( new ID("i"), new Entero( "1" ) )), new Imprime( new ID("i"))));
        }

        static Nodo *Arbol10()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("c", new ID("i", new ID("j")))), new Asignacion( new ID("a"), new Entero( "5" ) , new Asignacion( new ID("b"), new Entero( "23" ) , new Asignacion( new ID("c"), new Entero( "6" ) , new Asignacion( new ID("j"), new Entero( "7" ) , new Si( new Conjuncion( new Relacional("<=", new ID("a"), new Entero( "10" ) ), new Disyuncion( new Relacional(">=", new ID("b"), new Entero( "20" ) ), new Relacional("!=", new ID("c"), new Entero( "30" ) ))), new Asignacion( new ID("j"), new Suma( new ID("j"), new Entero( "2" ) )), new Asignacion( new ID("j"), new Entero( "0" ) ), new Para( new Asignacion( new ID("i"), new ID("j")), new Relacional("<", new ID("i"), new Entero( "10" ) ), new Asignacion( new ID("i"), new Suma( new ID("i"), new Entero( "1" ) )), new Imprime( new ID("i")))))))));
        }

        static Nodo *Arbol10v2()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("c", new ID("i", new ID("j", new ID("b"))))), new Asignacion( new ID("a"), new Entero( "5" ) , new Asignacion( new ID("b"), new Entero( "23" ) , new Asignacion( new ID("c"), new Entero( "6" ) , new Asignacion( new ID("j"), new Entero( "7" ) , new Si( new Conjuncion( new Relacional("<=", new ID("a"), new Entero( "10" ) ), new Disyuncion( new Relacional(">=", new ID("b"), new Entero( "20" ) ), new Relacional("!=", new ID("c"), new Entero( "30" ) ))), new Asignacion( new ID("j"), new Suma( new ID("j"), new Entero( "2" ) )), new Asignacion( new ID("j"), new Entero( "0" ) ), new Para( new Asignacion( new ID("i"), new ID("j")), new Relacional("<", new ID("i"), new Entero( "10" ) ), new Asignacion( new ID("i"), new Suma( new ID("i"), new Entero( "1" ) )), new Imprime( new ID("i")))))))));
        }

        static Nodo *Arbol0e()
        {
            return new Variables( new Tipo("int"), new ID("a"), new Variables( new Tipo("float"), new ID("x"), new Asignacion( new ID("a"), new ID("x"))));
        }

        static Nodo *Arbol1e()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b", new ID("c"))), new Asignacion( new ID("a"), new Entero( "2" ) , new Asignacion( new ID("b"), new Mult( new Entero( "3" ) , new ID("a")), new Asignacion( new ID("c"), new Suma( new ID("a"), new Mult( new ID("b"), new ID("d"))), new Imprime( new ID("c"))))));
        }

        static Nodo *Arbol2e()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b", new ID("c"))), new Variables( new Tipo("float"), new ID("d"), new Asignacion( new ID("a"), new Suma( new Entero( "2" ) , new Mult( new Entero( "3" ) , new ID("d"))), new Imprime( new ID("a")))));
        }

        static Nodo *Arbol3e()
        {
            return new Variables( new Tipo("int"), new ID("a", new ID("b", new ID("c"))), new Asignacion( new ID("b"), new Entero( "2" ) , new Asignacion( new ID("c"), new Entero( "3" ) , new Asignacion( new ID("a"), new Suma( new ID("b"), new Mult( new ID("c"), new Real( "2.5" ) )), new Imprime( new ID("a"))))));
        }
};

#endif
